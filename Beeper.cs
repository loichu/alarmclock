/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * *
* Projet  : Dream Machine Alarm Clock
* Auteur  : Loïc Humbert
* Desc.   : Simulateur du radio réveil ICF-C71PJ Dream Machine de Sony
* Version : 1.0
* Date    : octobre 2018
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * */
using System;

namespace AlarmClock
{
    public class Beeper
    {
        private static readonly TimeSpan DEFAULT_TIME = new TimeSpan(0,0,0);
        private static readonly Days DEFAULT_DAYS = Days.Weekdays;
        private static readonly TimeSpan SNOOZE_DURATION = new TimeSpan(0, 10, 0);

        // Alarm state
        private AlarmState _state;
        
        // Time when alarm will ring
        private TimeSpan _ringTime;
        
        // Time initially set
        private TimeSpan _originalTime;
        
        // Days when alarm will ring
        private Days _ringDays;
        
        // True if it shouldn't ring even if active
        private bool _locked;
        
        // PROPERTIES
        
        public AlarmState State
        {
            get { return _state; }
            set { _state = value; }
        }
        
        public TimeSpan RingTime
        {
            get { return _ringTime; }
            set { _ringTime = value; }
        }
        
        public TimeSpan OriginalTime
        {
            get { return _originalTime; }
            set
            {
                _originalTime = value; 
                _ringTime = value; 
            }
        }
        
        public Days RingDays
        {
            get { return _ringDays; }
            set { _ringDays = value; }
        }

        public bool Locked
        {
            get { return _locked; }
            set { _locked = value; }
        }

        // END PROPERTIES

        // Default constructor
        public Beeper() : this(DEFAULT_TIME, DEFAULT_DAYS){}

        /**
         * Specific constructor:
         * Sets the given ringTime and ringDays
         * Sets state to inactive
         * Sets locked to false
         */
        public Beeper(TimeSpan ringTime, Days ringDays)
        {
            this.OriginalTime = ringTime;
            this.RingDays = ringDays;
            this.State = AlarmState.Inactive;
            this.Locked = false;
        }

        /**
         * Set:
         * Activate beeper with given ringTime and ringDay
         */
        public void Set(TimeSpan ringTime, Days ringDays)
        {
            this.OriginalTime = ringTime;
            this.RingDays = ringDays;
            this.State = AlarmState.Active;
        }
        
        /**
         * Set:
         * Activate beeper with current ringTime and ringDay
         */
        public void Set()
        {
            this.State = AlarmState.Active;
        }

        // Add 1 minute to ringtime
        public void IncreaseMinutes()
        {
            this.OriginalTime += new TimeSpan(0, 1, 0);
        }

        // Take 1 minute to ringtime
        public void DecreaseMinutes()
        {
            this.OriginalTime -= new TimeSpan(0, 1, 0);
        }

        // Add 1 hour to ringtime
        public void IncreaseHours()
        {
            this.OriginalTime += new TimeSpan(1, 0, 0);
        }

        // Take 1 hour to ringtime
        public void DecreaseHours()
        {
            this.OriginalTime -= new TimeSpan(1, 0, 0);
        }

        // Dynamically toggle ring days
        public void ToggleRingDay()
        {
            int current_day = (int)this.RingDays;
            int next_day = current_day + 1;
            int max_value = Enum.GetNames(typeof(Days)).Length - 1;
            if (next_day > max_value)
                next_day = 0;
            this.RingDays = (Days)next_day;
        }

        /**
         * Reset:
         * Puts back original time as ring time
         * Sets state to reseted so it wont ring again
         */
        public void Reset()
        {
            this.RingTime = this.OriginalTime;
            this.State = AlarmState.Reseted;
        }

        /**
         * Snooze:
         * Adds SNOOZE_DURATION to ring time if alarm is ringing
         */
        public void Snooze()
        {
            if (this.State == AlarmState.Ringing)
            {
                this.RingTime += SNOOZE_DURATION;
                this.State = AlarmState.Active;
            }
        }

        /**
         * Toggle:
         * Switches between active and inactive states
         */
        public void Toggle()
        {
            if (this.State == AlarmState.Inactive)
            {
                this.State = AlarmState.Active;
            }
            else
            {
                this.State = AlarmState.Inactive;
            }
        }
    }
}