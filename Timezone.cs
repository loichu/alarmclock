/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * *
* Projet  : Dream Machine Alarm Clock
* Auteur  : Loïc Humbert
* Desc.   : Simulateur du radio réveil ICF-C71PJ Dream Machine de Sony
* Version : 1.0
* Date    : octobre 2018
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * */
namespace AlarmClock
{
    public enum Timezone
    {
        WesternEurope,
        CentralEurope,
        EasternEurope
    }
}