﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * *
* Projet  : Dream Machine Alarm Clock
* Auteur  : Loïc Humbert
* Desc.   : Simulateur du radio réveil ICF-C71PJ Dream Machine de Sony
* Version : 1.0
* Date    : octobre 2018
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * */
using System;
using System.Windows.Forms;

namespace AlarmClock
{
    public enum ViewMode
    {
        Display_Time,
        Display_Year,
        Display_DayMonth,
        Alarm_A_Hours,
        Alarm_A_Minutes,
        Alarm_A_Days,
        Alarm_B_Hours,
        Alarm_B_Minutes,
        Alarm_B_Days,
        Set_Year,
        Set_Day,
        Set_Month,
        Set_Hours,
        Set_Minutes,
        Set_Timezone
    }
    
    public partial class View : Form
    {
        // Some constants
        private const int TIMER_INTERVAL = 1000; // milliseconds
        private const int LONGPRESS_DURATION = 1800; // milliseconds
        private const int DISPLAY_TIME_AFTER = 3; // seconds

        // View state
        private ViewMode _viewMode = ViewMode.Display_Time;

        // Manage Alarms
        private AlarmClock _alarmClock = new AlarmClock();

        // Manage Time
        private DateTime _clockCentralEurope = DateTime.Now;
        private DateTime _clockEasternEurope;
        private DateTime _clockWesternEurope;
        private DateTime _currentTime;

        // To get buttons pressure duration
        private DateTime _btnAPressedAt;
        private DateTime _btnAReleasedAt;
        private DateTime _btnBPressedAt;
        private DateTime _btnBReleasedAt;
        private DateTime _btnDateTimezonePressedAt;
        private DateTime _btnDateTimezoneReleasedAt;
        private DateTime _btnDisplayClockPressedAt;
        private DateTime _btnDisplayClockReleasedAt;

        // To play sound
        private System.Media.SoundPlayer _player = new System.Media.SoundPlayer();

        // PROPERTIES

        private ViewMode ViewMode
        {
            get => _viewMode;
            set => _viewMode = value;
        }

        private AlarmClock AlarmClock
        {
            get => _alarmClock;
            set => _alarmClock = value;
        }

        private DateTime ClockCentralEurope
        {
            get => _clockCentralEurope;
            set => _clockCentralEurope = value;
        }

        private DateTime ClockEasternEurope
        {
            get => _clockEasternEurope;
            set => _clockEasternEurope = value;
        }

        private DateTime ClockWesternEurope
        {
            get => _clockWesternEurope;
            set => _clockWesternEurope = value;
        }

        private DateTime CurrentTime
        {
            get => _currentTime;
            set => _currentTime = value;
        }

        private DateTime BtnAPressedAt
        {
            get => _btnAPressedAt;
            set => _btnAPressedAt = value;
        }

        private DateTime BtnAReleasedAt
        {
            get => _btnAReleasedAt;
            set => _btnAReleasedAt = value;
        }

        private DateTime BtnBPressedAt
        {
            get => _btnBPressedAt;
            set => _btnBPressedAt = value;
        }

        private DateTime BtnBReleasedAt
        {
            get => _btnBReleasedAt;
            set => _btnBReleasedAt = value;
        }

        private DateTime BtnDateTimezonePressedAt
        {
            get => _btnDateTimezonePressedAt;
            set => _btnDateTimezonePressedAt = value;
        }

        private DateTime BtnDateTimezoneReleasedAt
        {
            get => _btnDateTimezoneReleasedAt;
            set => _btnDateTimezoneReleasedAt = value;
        }

        private DateTime BtnDisplayClockPressedAt
        {
            get => _btnDisplayClockPressedAt;
            set => _btnDisplayClockPressedAt = value;
        }

        private DateTime BtnDisplayClockReleasedAt
        {
            get => _btnDisplayClockReleasedAt;
            set => _btnDisplayClockReleasedAt = value;
        }

        private System.Media.SoundPlayer Player
        {
            get => _player;
            set => _player = value;
        }

        // END PROPERTIES

        /**
         * Constructor:
         * Initialize view
         * Set CurrentTime to ClockCentralEurope
         * Give value to other clocks
         * Set the timer
         * Display time
         */
        public View()
        {
            InitializeComponent();
            CurrentTime = ClockCentralEurope;
            ClockEasternEurope = ClockCentralEurope + new TimeSpan(1, 0, 0);
            ClockWesternEurope = ClockCentralEurope - new TimeSpan(1, 0, 0);
            Clock.Interval = TIMER_INTERVAL;
            Clock.Enabled = true;
            DisplayTime();
        }

        /**
         * UpdateView:
         * Handles the state transition between display date states and DispayTime
         * Call the display methods according to ViewMode
         */
        private void UpdateView()
        {
            // Display time if Date / Timezone button hasn't been pressed for DISPLAY_TIME_AFTER seconds
            int btn_hasnt_been_pressed_since = (int)(DateTime.UtcNow - BtnDateTimezoneReleasedAt).TotalSeconds;
            if ((ViewMode == ViewMode.Display_Year || ViewMode == ViewMode.Display_DayMonth)
                && btn_hasnt_been_pressed_since >= DISPLAY_TIME_AFTER
                )
                this.ViewMode = ViewMode.Display_Time;

            switch (ViewMode)
            {
                case ViewMode.Display_Time:
                case ViewMode.Set_Timezone:
                case ViewMode.Set_Hours:
                case ViewMode.Set_Minutes:
                    DisplayTime();
                    break;
                case ViewMode.Alarm_A_Hours:
                case ViewMode.Alarm_A_Minutes:
                case ViewMode.Alarm_A_Days:
                    DisplayAlarmA();
                    break;
                case ViewMode.Alarm_B_Hours:
                case ViewMode.Alarm_B_Minutes:
                case ViewMode.Alarm_B_Days:
                    DisplayAlarmB();
                    break;
                case ViewMode.Display_DayMonth:
                case ViewMode.Set_Month:
                case ViewMode.Set_Day:
                    DisplayDayMonth();
                    break;
                case ViewMode.Display_Year:
                case ViewMode.Set_Year:
                    DisplayYear();
                    break;
                default:
                    break;
            }
        }

        /**
         * UpdateTime:
         * Set the CurrentTime according to current Timezone
         */
        private void UpdateTime()
        {
            switch (AlarmClock.Timezone)
            {
                case Timezone.CentralEurope:
                    CurrentTime = ClockCentralEurope;
                    break;
                case Timezone.EasternEurope:
                    CurrentTime = ClockEasternEurope;
                    break;
                case Timezone.WesternEurope:
                    CurrentTime = ClockWesternEurope;
                    break;
                default:
                    break;
            }
        }

        /**
         * DisplayTime:
         * Updates the time displayed
         * Updates the day displayed
         * Asks AlarmClock to update alarms state
         * Updates the alarms status displayed
         */
        private void DisplayTime()
        {
            lblClock.Text = this.CurrentTime.TimeOfDay.ToString(@"hh\:mm");

            if (this.CurrentTime.DayOfWeek == DayOfWeek.Saturday
                || this.CurrentTime.DayOfWeek == DayOfWeek.Sunday
            )
            {
                lblWeekday.Visible = false;
                lblWeekend.Visible = true;
            }
            else // It is neither Sunday or Saturday so it is a weekday
            {
                lblWeekday.Visible = true;
                lblWeekend.Visible = false;
            }

            AlarmClock.SetState(CurrentTime);

            if (AlarmClock.GetState(Alarm.A) == AlarmState.Inactive)
            {
                lblAlarmA.Visible = false;
            }
            else
            {
                lblAlarmA.Visible = true;
            }

            if (AlarmClock.GetState(Alarm.B) == AlarmState.Inactive)
            {
                lblAlarmB.Visible = false;
            }
            else
            {
                lblAlarmB.Visible = true;
            }
        }

        private void DisplayAlarmA()
        {
            lblClock.Text = AlarmClock.GetTime(Alarm.A).ToString(@"hh\:mm");
            lblAlarmA.Visible = true;
            lblAlarmB.Visible = false;
            DisplayAlarmDay(Alarm.A);
        }

        private void DisplayAlarmB()
        {
            lblClock.Text = AlarmClock.GetTime(Alarm.B).ToString(@"hh\:mm");
            lblAlarmA.Visible = false;
            lblAlarmB.Visible = true;
            DisplayAlarmDay(Alarm.B);
        }

        private void DisplayYear()
        {
            lblClock.Text = CurrentTime.Year.ToString();
        }

        private void DisplayDayMonth()
        {
            lblClock.Text = $"{CurrentTime.Day}/{CurrentTime.Month}";
        }

        private void DisplayAlarmDay(Alarm alarm)
        {
            lblWeekday.Visible = AlarmClock.GetDays(alarm) == Days.Weekdays ? true : false;
            lblWeekend.Visible = AlarmClock.GetDays(alarm) == Days.Weekends ? true : false;
        }

        /**
         * Tick:
         * Callback function for the timer
         * Increment clocks
         * Call UpdateTime() 
         * Asks AlarmClock to check if it is ringing
         * Updates the WAKE UP label
         * Call UpdateView()
         */
        private void Tick(object sender, EventArgs e)
        {
            // Previous state
            bool was_ringing = AlarmClock.Ringing;

            // Update clock
            this.ClockCentralEurope += new TimeSpan(0, 0, 0, 0, TIMER_INTERVAL);
            this.ClockWesternEurope += new TimeSpan(0, 0, 0, 0, TIMER_INTERVAL);
            this.ClockEasternEurope += new TimeSpan(0, 0, 0, 0, TIMER_INTERVAL);
            UpdateTime(); // Sets the CurrentTime depending on the timezone

            // New state
            bool is_ringing = AlarmClock.CheckRing();

            // Control ring
            if (is_ringing)
            {
                lblRing.Visible = true;
                if (!was_ringing)
                {
                    PlayAlarm();
                }
            } else
            {
                lblRing.Visible = false;
                if (was_ringing)
                {
                    StopAlarm();
                }
            }
            
            UpdateView();            
        }

        /**
         * AlarmAReleased:
         * Detects click duration
         * Operates on states according to click duration
         * and current state (ViewMode)
         * Call UpdateView()
         */
        private void AlarmAReleased(object sender, EventArgs e)
        {
            this.BtnAReleasedAt = DateTime.UtcNow;
            uint pressure_duration = (uint)(BtnAReleasedAt - BtnAPressedAt).TotalMilliseconds;

            switch (ViewMode)
            {
                case ViewMode.Display_Time:
                    if (pressure_duration <= LONGPRESS_DURATION)
                    {
                        AlarmClock.ToggleAlarm(Alarm.A);
                    } else
                    {
                        ViewMode = ViewMode.Alarm_A_Hours;
                        AlarmClock.Lock(Alarm.A);
                    }
                    break;
                case ViewMode.Alarm_A_Hours:
                    ViewMode = ViewMode.Alarm_A_Minutes;
                    break;
                case ViewMode.Alarm_A_Minutes:
                    ViewMode = ViewMode.Alarm_A_Days;
                    break;
                case ViewMode.Alarm_A_Days:
                    // Display current time
                    ViewMode = ViewMode.Display_Time;
                    
                    // Make it ring if it should
                    AlarmClock.Unlock(Alarm.A);
                    
                    // Make it active
                    AlarmClock.SetAlarm(Alarm.A);
                    break;
                default:
                    AlarmClock.ToggleAlarm(Alarm.A);
                    break;
            }
            UpdateView();
        }

        /**
         * AlarmAReleased:
         * Detects click duration
         * Operates on states according to click duration
         * and current state (ViewMode)
         * Call UpdateView()
         */
        private void AlarmBReleased(object sender, EventArgs e)
        {
            this.BtnBReleasedAt = DateTime.UtcNow;
            uint pressure_duration = (uint)(BtnBReleasedAt - BtnBPressedAt).TotalMilliseconds;
            //Console.WriteLine($"Button B pressed during: {pressure_duration} ms");
            switch (ViewMode)
            {
                case ViewMode.Display_Time:
                    if (pressure_duration <= LONGPRESS_DURATION)
                    {
                        AlarmClock.ToggleAlarm(Alarm.B);
                    }
                    else
                    {
                        ViewMode = ViewMode.Alarm_B_Hours;
                        AlarmClock.Lock(Alarm.B);
                    }
                    break;
                case ViewMode.Alarm_B_Hours:
                    ViewMode = ViewMode.Alarm_B_Minutes;
                    break;
                case ViewMode.Alarm_B_Minutes:
                    ViewMode = ViewMode.Alarm_B_Days;
                    break;
                case ViewMode.Alarm_B_Days:
                    // Display current time
                    ViewMode = ViewMode.Display_Time;

                    // Make it ring if it should
                    AlarmClock.Unlock(Alarm.B);

                    // Make it active
                    AlarmClock.SetAlarm(Alarm.B);
                    break;
                default:
                    AlarmClock.ToggleAlarm(Alarm.B);
                    break;
            }
            UpdateView();
        }

        // Used to calculate click duration on btn set alarm A
        private void SetAlarmA(object sender, EventArgs e)
        {
            BtnAPressedAt = DateTime.UtcNow;
        }

        // Used to calculate click duration on btn set alarm B
        private void SetAlarmB(object sender, EventArgs e)
        {
            BtnBPressedAt = DateTime.UtcNow;
        }

        /**
         * TimeSetAPlus:
         * Increases alarm A in a alarm A setting state
         * Call general method to increase clocks
         * in a clock setting state
         * Call UpdateView()
         */
        private void TimeSetAPlus(object sender, EventArgs e)
        {
            switch (ViewMode)
            {
                case ViewMode.Alarm_A_Hours:
                    AlarmClock.IncreaseHours(Alarm.A);
                    break;
                case ViewMode.Alarm_A_Minutes:
                    AlarmClock.IncreaseMinutes(Alarm.A);
                    break;
                case ViewMode.Alarm_A_Days:
                    AlarmClock.ToggleRingDay(Alarm.A);
                    break;
                default:
                    GeneralSettingsPlus();
                    break;
            }
            UpdateView();
        }

        /**
         * TimeSetBPlus:
         * Increases alarm B in a alarm B setting state
         * Call general method to increase clocks
         * in a clock setting state
         * Call UpdateView()
         */
        private void TimeSetBPlus(object sender, EventArgs e)
        {
            switch (ViewMode)
            {
                case ViewMode.Alarm_B_Hours:
                    AlarmClock.IncreaseHours(Alarm.B);
                    break;
                case ViewMode.Alarm_B_Minutes:
                    AlarmClock.IncreaseMinutes(Alarm.B);
                    break;
                case ViewMode.Alarm_B_Days:
                    AlarmClock.ToggleRingDay(Alarm.B);
                    break;
                default:
                    GeneralSettingsPlus();
                    break;
            }
            UpdateView();
        }

        /**
         * TimeSetAMinus:
         * Decreases alarm A in a alarm A setting state
         * Call general method to decrease clocks
         * in a clock setting state
         * Call UpdateView()
         */
        private void TimeSetAMinus(object sender, EventArgs e)
        {
            switch (ViewMode)
            {
                case ViewMode.Alarm_A_Hours:
                    AlarmClock.DecreaseHours(Alarm.A);
                    break;
                case ViewMode.Alarm_A_Minutes:
                    AlarmClock.DecreaseMinutes(Alarm.A);
                    break;
                case ViewMode.Alarm_A_Days:
                    AlarmClock.ToggleRingDay(Alarm.A);
                    break;
                default:
                    GeneralSettingsMinus();
                    break;
            }
            UpdateView();
        }

        /**
         * TimeSetBMinus:
         * Decreases alarm B in a alarm B setting state
         * Call general method to decrease clocks
         * in a clock setting state
         * Call UpdateView()
         */
        private void TimeSetBMinus(object sender, EventArgs e)
        {
            switch (ViewMode)
            {
                case ViewMode.Alarm_B_Hours:
                    AlarmClock.DecreaseHours(Alarm.B);
                    break;
                case ViewMode.Alarm_B_Minutes:
                    AlarmClock.DecreaseMinutes(Alarm.B);
                    break;
                case ViewMode.Alarm_B_Days:
                    AlarmClock.ToggleRingDay(Alarm.B);
                    break;
                default:
                    GeneralSettingsMinus();
                    break;
            }
            UpdateView();
        }

        // Asks AlarmClock to snooze alarms
        private void Snooze(object sender, EventArgs e)
        {
            AlarmClock.Snooze();
        }

        // Asks AlarmClock to reset alarms
        private void Reset(object sender, EventArgs e)
        {
            AlarmClock.Reset();
        }

        // Used to calculate click duration on Date/Timezone button
        private void DateTimezonePressed(object sender, EventArgs e)
        {
            this.BtnDateTimezonePressedAt = DateTime.UtcNow;
        }

        /**
         * DateTimezoneReleased:
         * Detects click duration
         * Operates on states according to click duration
         * and current state (ViewMode)
         * Switches between year and day/month display
         * if the click is short
         * Call UpdateView()
         */
        private void DateTimezoneReleased(object sender, EventArgs e)
        {
            this.BtnDateTimezoneReleasedAt = DateTime.UtcNow;
            uint pressure_duration = (uint)(BtnDateTimezoneReleasedAt - BtnDateTimezonePressedAt).TotalMilliseconds;
            switch (ViewMode)
            {
                case ViewMode.Display_Year:
                    ViewMode = ViewMode.Display_DayMonth;
                    break;
                case ViewMode.Display_DayMonth:
                    ViewMode = ViewMode.Display_Year;
                    break;
                case ViewMode.Set_Timezone:
                    ViewMode = ViewMode.Display_Time;
                    break;
                default:
                    if (pressure_duration >= LONGPRESS_DURATION && ViewMode == ViewMode.Display_Time)
                    {
                        ViewMode = ViewMode.Set_Timezone;
                    }
                    else
                    {
                        ViewMode = ViewMode.Display_Year;
                    }
                    break;
            }
            UpdateView();
        }

        // Used to calculate click duration on Display/Clock button
        private void DisplayClockPressed(object sender, EventArgs e)
        {
            this.BtnDisplayClockPressedAt = DateTime.UtcNow;
        }

        /**
         * DisplayClockReleased:
         * Detects click duration
         * Operates on states according to click duration
         * and current state (ViewMode)
         * Call UpdateView()
         */
        private void DisplayClockReleased(object sender, EventArgs e)
        {
            this.BtnDisplayClockReleasedAt = DateTime.UtcNow;
            uint pressure_duration = (uint)(BtnDisplayClockReleasedAt - BtnDisplayClockPressedAt).TotalMilliseconds;
            switch (ViewMode)
            {
                case ViewMode.Display_Time:
                    if (pressure_duration >= LONGPRESS_DURATION)
                        ViewMode = ViewMode.Set_Year;
                    break;
                case ViewMode.Set_Year:
                    ViewMode = ViewMode.Set_Day;
                    break;
                case ViewMode.Set_Day:
                    ViewMode = ViewMode.Set_Month;
                    break;
                case ViewMode.Set_Month:
                    ViewMode = ViewMode.Set_Hours;
                    break;
                case ViewMode.Set_Hours:
                    ViewMode = ViewMode.Set_Minutes;
                    break;
                default:
                    ViewMode = ViewMode.Display_Time;
                    break;
            }
            UpdateView();
        }

        // General method to decrease clocks in a clock setting state
        private void GeneralSettingsMinus()
        {
            switch (ViewMode)
            {
                case ViewMode.Set_Timezone:
                    AlarmClock.PreviousTimezone();
                    break;
                case ViewMode.Set_Year:
                    ClockCentralEurope = ClockCentralEurope.AddYears(-1);
                    ClockWesternEurope = ClockWesternEurope.AddYears(-1);
                    ClockEasternEurope = ClockEasternEurope.AddYears(-1);
                    break;
                case ViewMode.Set_Month:
                    ClockCentralEurope = ClockCentralEurope.AddMonths(-1);
                    ClockWesternEurope = ClockWesternEurope.AddMonths(-1);
                    ClockEasternEurope = ClockEasternEurope.AddMonths(-1);
                    break;
                case ViewMode.Set_Day:
                    ClockCentralEurope = ClockCentralEurope.AddDays(-1);
                    ClockWesternEurope = ClockWesternEurope.AddDays(-1);
                    ClockEasternEurope = ClockEasternEurope.AddDays(-1);
                    break;
                case ViewMode.Set_Hours:
                    ClockCentralEurope = ClockCentralEurope.AddHours(-1);
                    ClockWesternEurope = ClockWesternEurope.AddHours(-1);
                    ClockEasternEurope = ClockEasternEurope.AddHours(-1);
                    break;
                case ViewMode.Set_Minutes:
                    ClockCentralEurope = ClockCentralEurope.AddMinutes(-1);
                    ClockWesternEurope = ClockWesternEurope.AddMinutes(-1);
                    ClockEasternEurope = ClockEasternEurope.AddMinutes(-1);
                    break;
                default:
                    break;
            }
        }

        // General method to increase clocks in a clock setting state
        private void GeneralSettingsPlus()
        {
            switch (ViewMode)
            {
                case ViewMode.Set_Timezone:
                    AlarmClock.NextTimezone();
                    break;
                case ViewMode.Set_Year:
                    ClockCentralEurope = ClockCentralEurope.AddYears(1);
                    ClockWesternEurope = ClockWesternEurope.AddYears(1);
                    ClockEasternEurope = ClockEasternEurope.AddYears(1);
                    break;
                case ViewMode.Set_Month:
                    ClockCentralEurope = ClockCentralEurope.AddMonths(1);
                    ClockWesternEurope = ClockWesternEurope.AddMonths(1);
                    ClockEasternEurope = ClockEasternEurope.AddMonths(1);
                    break;
                case ViewMode.Set_Day:
                    ClockCentralEurope = ClockCentralEurope.AddDays(1);
                    ClockWesternEurope = ClockWesternEurope.AddDays(1);
                    ClockEasternEurope = ClockEasternEurope.AddDays(1);
                    break;
                case ViewMode.Set_Hours:
                    ClockCentralEurope = ClockCentralEurope.AddHours(1);
                    ClockWesternEurope = ClockWesternEurope.AddHours(1);
                    ClockEasternEurope = ClockEasternEurope.AddHours(1);
                    break;
                case ViewMode.Set_Minutes:
                    ClockCentralEurope = ClockCentralEurope.AddMinutes(1);
                    ClockWesternEurope = ClockWesternEurope.AddMinutes(1);
                    ClockEasternEurope = ClockEasternEurope.AddMinutes(1);
                    break;
                default:
                    break;
            }
        }

        // Plays the sound in loop
        private void PlayAlarm()
        {
            _player.Stream = Properties.Resources.AlarmSound;
            _player.PlayLooping();
        }

        // Stops the sound in loop
        private void StopAlarm()
        {
            _player.Stop();
        }
    }
}