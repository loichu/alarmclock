﻿using System.Windows.Forms;

namespace AlarmClock
{
    partial class View
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        
        //private void LongPress

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTimeSetBMinus = new System.Windows.Forms.Button();
            this.btnTimeSetBPlus = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTimeSetAMinus = new System.Windows.Forms.Button();
            this.btnAlarmReset = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAlarmB = new System.Windows.Forms.Button();
            this.btnAlarmA = new System.Windows.Forms.Button();
            this.btnTimeSetAPlus = new System.Windows.Forms.Button();
            this.btnSnoozeBrightness = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblAlarmB = new System.Windows.Forms.Label();
            this.lblAlarmA = new System.Windows.Forms.Label();
            this.lblRing = new System.Windows.Forms.Label();
            this.lblWeekend = new System.Windows.Forms.Label();
            this.lblWeekday = new System.Windows.Forms.Label();
            this.lblClock = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnDateTimezone = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.btnDisplayClock = new System.Windows.Forms.Button();
            this.Clock = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnTimeSetBMinus);
            this.groupBox1.Controls.Add(this.btnTimeSetBPlus);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnTimeSetAMinus);
            this.groupBox1.Controls.Add(this.btnAlarmReset);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnAlarmB);
            this.groupBox1.Controls.Add(this.btnAlarmA);
            this.groupBox1.Controls.Add(this.btnTimeSetAPlus);
            this.groupBox1.Controls.Add(this.btnSnoozeBrightness);
            this.groupBox1.Location = new System.Drawing.Point(94, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(890, 243);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TOP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(724, 85);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "TIME SET";
            // 
            // btnTimeSetBMinus
            // 
            this.btnTimeSetBMinus.Location = new System.Drawing.Point(668, 77);
            this.btnTimeSetBMinus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTimeSetBMinus.Name = "btnTimeSetBMinus";
            this.btnTimeSetBMinus.Size = new System.Drawing.Size(48, 35);
            this.btnTimeSetBMinus.TabIndex = 10;
            this.btnTimeSetBMinus.Text = "-";
            this.btnTimeSetBMinus.UseVisualStyleBackColor = true;
            this.btnTimeSetBMinus.Click += new System.EventHandler(this.TimeSetBMinus);
            // 
            // btnTimeSetBPlus
            // 
            this.btnTimeSetBPlus.Location = new System.Drawing.Point(816, 77);
            this.btnTimeSetBPlus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTimeSetBPlus.Name = "btnTimeSetBPlus";
            this.btnTimeSetBPlus.Size = new System.Drawing.Size(48, 35);
            this.btnTimeSetBPlus.TabIndex = 9;
            this.btnTimeSetBPlus.Text = "+";
            this.btnTimeSetBPlus.UseVisualStyleBackColor = true;
            this.btnTimeSetBPlus.Click += new System.EventHandler(this.TimeSetBPlus);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 92);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "TIME SET";
            // 
            // btnTimeSetAMinus
            // 
            this.btnTimeSetAMinus.Location = new System.Drawing.Point(27, 85);
            this.btnTimeSetAMinus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTimeSetAMinus.Name = "btnTimeSetAMinus";
            this.btnTimeSetAMinus.Size = new System.Drawing.Size(48, 35);
            this.btnTimeSetAMinus.TabIndex = 7;
            this.btnTimeSetAMinus.Text = "-";
            this.btnTimeSetAMinus.UseVisualStyleBackColor = true;
            this.btnTimeSetAMinus.Click += new System.EventHandler(this.TimeSetAMinus);
            // 
            // btnAlarmReset
            // 
            this.btnAlarmReset.Location = new System.Drawing.Point(478, 50);
            this.btnAlarmReset.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAlarmReset.Name = "btnAlarmReset";
            this.btnAlarmReset.Size = new System.Drawing.Size(69, 34);
            this.btnAlarmReset.TabIndex = 6;
            this.btnAlarmReset.Text = "OFF";
            this.btnAlarmReset.UseVisualStyleBackColor = true;
            this.btnAlarmReset.Click += new System.EventHandler(this.Reset);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(458, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "ALARM RESET";
            // 
            // btnAlarmB
            // 
            this.btnAlarmB.Location = new System.Drawing.Point(686, 30);
            this.btnAlarmB.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAlarmB.Name = "btnAlarmB";
            this.btnAlarmB.Size = new System.Drawing.Size(159, 35);
            this.btnAlarmB.TabIndex = 4;
            this.btnAlarmB.Text = "B ALARM ON/OFF";
            this.btnAlarmB.UseVisualStyleBackColor = true;
            this.btnAlarmB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SetAlarmB);
            this.btnAlarmB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AlarmBReleased);
            // 
            // btnAlarmA
            // 
            this.btnAlarmA.Location = new System.Drawing.Point(40, 30);
            this.btnAlarmA.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAlarmA.Name = "btnAlarmA";
            this.btnAlarmA.Size = new System.Drawing.Size(159, 35);
            this.btnAlarmA.TabIndex = 3;
            this.btnAlarmA.Text = "A ALARM ON/OFF";
            this.btnAlarmA.UseVisualStyleBackColor = true;
            this.btnAlarmA.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SetAlarmA);
            this.btnAlarmA.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AlarmAReleased);
            // 
            // btnTimeSetAPlus
            // 
            this.btnTimeSetAPlus.Location = new System.Drawing.Point(176, 85);
            this.btnTimeSetAPlus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTimeSetAPlus.Name = "btnTimeSetAPlus";
            this.btnTimeSetAPlus.Size = new System.Drawing.Size(48, 35);
            this.btnTimeSetAPlus.TabIndex = 1;
            this.btnTimeSetAPlus.Text = "+";
            this.btnTimeSetAPlus.UseVisualStyleBackColor = true;
            this.btnTimeSetAPlus.Click += new System.EventHandler(this.TimeSetAPlus);
            // 
            // btnSnoozeBrightness
            // 
            this.btnSnoozeBrightness.Location = new System.Drawing.Point(176, 152);
            this.btnSnoozeBrightness.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSnoozeBrightness.Name = "btnSnoozeBrightness";
            this.btnSnoozeBrightness.Size = new System.Drawing.Size(540, 66);
            this.btnSnoozeBrightness.TabIndex = 0;
            this.btnSnoozeBrightness.Text = "SNOOZE / BRIGHTNESS";
            this.btnSnoozeBrightness.UseVisualStyleBackColor = true;
            this.btnSnoozeBrightness.Click += new System.EventHandler(this.Snooze);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblAlarmB);
            this.groupBox2.Controls.Add(this.lblAlarmA);
            this.groupBox2.Controls.Add(this.lblRing);
            this.groupBox2.Controls.Add(this.lblWeekend);
            this.groupBox2.Controls.Add(this.lblWeekday);
            this.groupBox2.Controls.Add(this.lblClock);
            this.groupBox2.Location = new System.Drawing.Point(94, 290);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(890, 243);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "FRONT";
            // 
            // lblAlarmB
            // 
            this.lblAlarmB.AutoSize = true;
            this.lblAlarmB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlarmB.Location = new System.Drawing.Point(810, 152);
            this.lblAlarmB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAlarmB.Name = "lblAlarmB";
            this.lblAlarmB.Size = new System.Drawing.Size(25, 25);
            this.lblAlarmB.TabIndex = 6;
            this.lblAlarmB.Text = "B";
            // 
            // lblAlarmA
            // 
            this.lblAlarmA.AutoSize = true;
            this.lblAlarmA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlarmA.Location = new System.Drawing.Point(36, 155);
            this.lblAlarmA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAlarmA.Name = "lblAlarmA";
            this.lblAlarmA.Size = new System.Drawing.Size(26, 25);
            this.lblAlarmA.TabIndex = 4;
            this.lblAlarmA.Text = "A";
            // 
            // lblRing
            // 
            this.lblRing.AutoSize = true;
            this.lblRing.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRing.Location = new System.Drawing.Point(154, 140);
            this.lblRing.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRing.Name = "lblRing";
            this.lblRing.Size = new System.Drawing.Size(73, 50);
            this.lblRing.TabIndex = 3;
            this.lblRing.Text = "WAKE\r\nUP";
            this.lblRing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWeekend
            // 
            this.lblWeekend.AutoSize = true;
            this.lblWeekend.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeekend.Location = new System.Drawing.Point(724, 38);
            this.lblWeekend.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWeekend.Name = "lblWeekend";
            this.lblWeekend.Size = new System.Drawing.Size(115, 25);
            this.lblWeekend.TabIndex = 2;
            this.lblWeekend.Text = "WEEKEND";
            // 
            // lblWeekday
            // 
            this.lblWeekday.AutoSize = true;
            this.lblWeekday.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeekday.Location = new System.Drawing.Point(609, 38);
            this.lblWeekday.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWeekday.Name = "lblWeekday";
            this.lblWeekday.Size = new System.Drawing.Size(112, 25);
            this.lblWeekday.TabIndex = 1;
            this.lblWeekday.Text = "WEEKDAY";
            // 
            // lblClock
            // 
            this.lblClock.AutoSize = true;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClock.Location = new System.Drawing.Point(330, 85);
            this.lblClock.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(168, 64);
            this.lblClock.TabIndex = 0;
            this.lblClock.Text = "00:00";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.btnDateTimezone);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.btnDisplayClock);
            this.groupBox3.Location = new System.Drawing.Point(94, 603);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(890, 243);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "REAR";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(302, 38);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 30);
            this.label9.TabIndex = 5;
            this.label9.Text = "DATE/\r\nTIME ZONE";
            // 
            // btnDateTimezone
            // 
            this.btnDateTimezone.Location = new System.Drawing.Point(304, 70);
            this.btnDateTimezone.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDateTimezone.Name = "btnDateTimezone";
            this.btnDateTimezone.Size = new System.Drawing.Size(62, 22);
            this.btnDateTimezone.TabIndex = 4;
            this.btnDateTimezone.UseVisualStyleBackColor = true;
            this.btnDateTimezone.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DateTimezonePressed);
            this.btnDateTimezone.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DateTimezoneReleased);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(212, 38);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 30);
            this.label8.TabIndex = 3;
            this.label8.Text = "DISPLAY/\r\nCLOCK";
            // 
            // btnDisplayClock
            // 
            this.btnDisplayClock.Location = new System.Drawing.Point(214, 70);
            this.btnDisplayClock.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDisplayClock.Name = "btnDisplayClock";
            this.btnDisplayClock.Size = new System.Drawing.Size(62, 22);
            this.btnDisplayClock.TabIndex = 2;
            this.btnDisplayClock.UseVisualStyleBackColor = true;
            this.btnDisplayClock.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DisplayClockPressed);
            this.btnDisplayClock.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DisplayClockReleased);
            // 
            // Clock
            // 
            this.Clock.Interval = 1000;
            this.Clock.Tick += new System.EventHandler(this.Tick);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 900);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "View";
            this.Text = "Dream Machine";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnAlarmB;
        private System.Windows.Forms.Button btnAlarmA;
        private System.Windows.Forms.Button btnTimeSetAPlus;
        private System.Windows.Forms.Button btnSnoozeBrightness;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblAlarmB;
        private System.Windows.Forms.Label lblAlarmA;
        private System.Windows.Forms.Label lblRing;
        private System.Windows.Forms.Label lblWeekend;
        private System.Windows.Forms.Label lblWeekday;
        private System.Windows.Forms.Label lblClock;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnDateTimezone;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnDisplayClock;
        private System.Windows.Forms.Button btnAlarmReset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer Clock;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTimeSetBMinus;
        private System.Windows.Forms.Button btnTimeSetBPlus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnTimeSetAMinus;
    }
}

