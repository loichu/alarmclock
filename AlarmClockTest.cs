﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * *
* Projet  : Dream Machine Alarm Clock
* Auteur  : Loïc Humbert
* Desc.   : Simulateur du radio réveil ICF-C71PJ Dream Machine de Sony
* Version : 1.0
* Date    : octobre 2018
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * */
using System;
using Xunit;

namespace AlarmClock
{
    public class AlarmClockTest
    {
        public class NoAlarmSet
        {
            [Fact]
            public void TestTwoAlarms()
            {
                Assert.Equal(2, new AlarmClock().Alarms.Count);
            }

            [Fact]
            public void TestAlarmsOff()
            {
                foreach (var alarm in new AlarmClock().Alarms)
                {
                    Assert.True(alarm.State == AlarmState.Inactive);
                }
            }

            [Fact]
            public void TestAlarmsZero()
            {
                foreach (var alarm in new AlarmClock().Alarms)
                {
                    Assert.Equal(new TimeSpan(0, 0, 0), alarm.RingTime);
                }
            }

            [Fact]
            public void TestAlarmsWeekdays()
            {
                foreach (var alarm in new AlarmClock().Alarms)
                {
                    Assert.Equal(Days.Weekdays, alarm.RingDays);
                }
            }

            [Fact]
            public void TestSetAlarm()
            {
                AlarmClock alarm_clock = new AlarmClock();

                TimeSpan ring_time = new TimeSpan(8, 0, 0);

                alarm_clock.SetAlarm(Alarm.A, ring_time, Days.Weekdays);

                Assert.False(alarm_clock.Alarms[(int)Alarm.A].State == AlarmState.Inactive);
                Assert.True(alarm_clock.Alarms[(int)Alarm.B].State == AlarmState.Inactive);
            }

            [Fact]
            public void TestToggleAlarm()
            {
                AlarmClock alarm_clock = new AlarmClock();

                alarm_clock.ToggleAlarm(Alarm.A);

                Assert.False(alarm_clock.Alarms[(int)Alarm.A].State == AlarmState.Inactive);
                Assert.True(alarm_clock.Alarms[(int)Alarm.B].State == AlarmState.Inactive);

                alarm_clock.ToggleAlarm(Alarm.A);

                Assert.True(alarm_clock.Alarms[(int)Alarm.A].State == AlarmState.Inactive);
                Assert.True(alarm_clock.Alarms[(int)Alarm.B].State == AlarmState.Inactive);
            }
        }

        public class AlarmASet
        {
            private AlarmClock AlarmClock;

            private DateTime OneMinuteBefore;
            private DateTime RingTime;
            private DateTime OneMinuteAfter;
            private TimeSpan RingDuration;
            private TimeSpan SnoozeShift;

            public AlarmASet()
            {

                this.AlarmClock = new AlarmClock();

                this.RingTime = new DateTime(2018, 1, 9, 8, 0, 0);
                this.OneMinuteAfter = this.RingTime + new TimeSpan(0, 1, 0);
                this.OneMinuteBefore = this.RingTime - new TimeSpan(0, 1, 0);
                this.RingDuration = new TimeSpan(0, 5, 0);
                this.SnoozeShift = new TimeSpan(0, 10, 0);

                AlarmClock.SetAlarm(Alarm.A, this.RingTime.TimeOfDay, Days.Weekdays);
            }

            [Fact]
            public void TestAlarmRingAtTime()
            {
                AlarmClock.SetState(this.OneMinuteBefore);
                Assert.False(AlarmClock.CheckRing());
                AlarmClock.SetState(this.RingTime);
                Assert.True(AlarmClock.CheckRing());
            }

            [Fact]
            public void TestAlarmRingAfter()
            {
                AlarmClock.SetState(this.OneMinuteBefore);
                Assert.False(AlarmClock.CheckRing());
                AlarmClock.SetState(this.OneMinuteBefore + this.RingDuration);
                Assert.True(AlarmClock.CheckRing());
                AlarmClock.SetState(
                    this.RingTime
                    + this.RingDuration
                    + new TimeSpan(0, 0, 1)
                );
                Assert.False(AlarmClock.CheckRing());
            }

            [Fact]
            public void TestAlarmReset()
            {
                AlarmClock.SetState(this.RingTime);
                Assert.True(AlarmClock.CheckRing());
                AlarmClock.Reset();
                Assert.False(AlarmClock.CheckRing());
                Beeper alarm_a = AlarmClock.Alarms[(int)Alarm.A];
                Assert.Equal(alarm_a.RingTime, alarm_a.OriginalTime);
            }

            [Fact]
            public void TestAlarmSnooze()
            {
                AlarmClock.SetState(this.RingTime);
                Assert.True(AlarmClock.CheckRing());
                AlarmClock.Snooze();
                Assert.False(AlarmClock.CheckRing());
                Beeper alarm_a = AlarmClock.Alarms[(int)Alarm.A];
                Assert.Equal(alarm_a.RingTime, alarm_a.OriginalTime + SnoozeShift);
            }

            [Fact]
            public void TestAlarmNotRingingSnooze()
            {
                AlarmClock.SetAlarm(Alarm.A, new TimeSpan(23, 0, 0), Days.Weekends);
                Assert.False(AlarmClock.CheckRing());
                AlarmClock.Snooze();
                Assert.Equal(AlarmClock.Alarms[(int)Alarm.A].RingTime, AlarmClock.Alarms[(int)Alarm.A].OriginalTime);
            }
        }

        [Fact]
        public void ToggleRingDays()
        {
            AlarmClock clock = new AlarmClock();
            Assert.Equal(Days.Weekdays, clock.Alarms[(int)Alarm.A].RingDays);
            clock.ToggleRingDay(Alarm.A);
            Assert.Equal(Days.Weekends, clock.Alarms[(int)Alarm.A].RingDays);
            clock.ToggleRingDay(Alarm.A);
            Assert.Equal(Days.Weekdays, clock.Alarms[(int)Alarm.A].RingDays);
        }

        [Fact]
        public void TestTimezones()
        {
            Timezone test_tz0 = Timezone.WesternEurope;
            Timezone test_tz1 = Timezone.CentralEurope;
            Timezone test_tz2 = Timezone.EasternEurope;
            Assert.Equal(0, (int)test_tz0);
            Assert.Equal(1, (int)test_tz1);
            Assert.Equal(2, (int)test_tz2);
        }

        [Fact]
        public void TestChangeTimezone()
        {
            AlarmClock clock = new AlarmClock();
            Assert.Equal(Timezone.CentralEurope, clock.Timezone);
            clock.NextTimezone();
            Assert.Equal(Timezone.EasternEurope, clock.Timezone);
            clock.NextTimezone();
            Assert.Equal(Timezone.WesternEurope, clock.Timezone);
            clock.NextTimezone();
            Assert.Equal(Timezone.CentralEurope, clock.Timezone);
            clock.PreviousTimezone();
            Assert.Equal(Timezone.WesternEurope, clock.Timezone);
            clock.PreviousTimezone();
            Assert.Equal(Timezone.EasternEurope, clock.Timezone);
            clock.PreviousTimezone();
            Assert.Equal(Timezone.CentralEurope, clock.Timezone);
        }
    }
}
