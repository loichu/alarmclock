﻿/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * *
* Projet  : Dream Machine Alarm Clock
* Auteur  : Loïc Humbert
* Desc.   : Simulateur du radio réveil ICF-C71PJ Dream Machine de Sony
* Version : 1.0
* Date    : octobre 2018
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * */
using  System;
using System.Collections.Generic;

namespace AlarmClock
{
    public class AlarmClock
    {
        private static readonly TimeSpan RING_DURATION = new TimeSpan(0, 5, 0);
        
        // Handle all alarms
        private List<Beeper> _alarms;
        
        // Handle timezones
        private Timezone _timezone;
        
        // Detect state transition Active -> Ringing
        private bool _ringing;

        // PROPERTIES
        
        public List<Beeper> Alarms
        {
            get => _alarms;
            private set => _alarms = value;
        }

        public Timezone Timezone
        {
            get => _timezone;
            private set => _timezone = value;
        }

        public bool Ringing
        {
            get => _ringing;
            private set => _ringing = value;
        }
        
        // END PROPERTIES

        // AlarmClock default constructor
        public AlarmClock() : this(Timezone.CentralEurope) {}

        /**
         * AlarmClock specific constructor
         * Could be used to sell alarm clocks to other countries
         * Initialize alarms
         * Set the timezone
         */
        public AlarmClock(Timezone timezone)
        {
            this.Alarms = new List<Beeper>{new Beeper(), new Beeper()};
            this.Timezone = timezone;
        }

        /**
         * SetAlarm:
         * Turn on alarm with specified time and days
         */
        public void SetAlarm(Alarm alarm, TimeSpan ring_time, Days days)
        {
            this.Alarms[(int) alarm].Set(ring_time, days);
        }
        
        /**
         * SetAlarm:
         * Turn on alarm with current time and days
         */
        public void SetAlarm(Alarm alarm)
        {
            this.Alarms[(int) alarm].Set();
        }

        /**
         * ToggleAlarm:
         * Switches between alarm on/off
         */
        public void ToggleAlarm(Alarm alarm)
        {
            this.Alarms[(int) alarm].Toggle();
        }

        /**
         * SetState:
         * Takes the current time
         * Operates on all alarms
         * Given the current time and current alarm states:
         * Operates on alarms states
         */
        public void SetState(DateTime CurrentTime)
        {
            foreach (Beeper alarm in Alarms)
            {
                // Skip if inactive
                if (alarm.State == AlarmState.Inactive) continue;

                // Check if ringing
                if (alarm.State == AlarmState.Active
                    && CurrentTime.TimeOfDay >= alarm.RingTime
                    && CurrentTime.TimeOfDay <= alarm.RingTime + RING_DURATION
                    /*&& !alarm.Locked*/)
                {
                    if (CurrentTime.DayOfWeek == DayOfWeek.Saturday
                        || CurrentTime.DayOfWeek == DayOfWeek.Sunday
                    )
                    {
                        if (alarm.RingDays == Days.Weekends)
                            alarm.State = AlarmState.Ringing;
                    }
                    else
                    {
                        if (alarm.RingDays == Days.Weekdays)
                            alarm.State = AlarmState.Ringing;
                    }
                }

                // Set to Active if needed
                if ((alarm.State == AlarmState.Reseted 
                    || alarm.State == AlarmState.Ringing) 
                    && CurrentTime.TimeOfDay >= alarm.RingTime + RING_DURATION
                    )
                    alarm.State = AlarmState.Active;
            }
        }

        /**
         * GetState:
         * Returns alarm current state
         */
        public AlarmState GetState(Alarm alarm)
        {
            return Alarms[(int)alarm].State;
        }

        /**
         * CheckRing:
         * Checks if an alarm is ringing
         */
        public bool CheckRing()
        {
            foreach (Beeper alarm in Alarms)
            {
                if (alarm.State == AlarmState.Ringing && !alarm.Locked)
                {
                    this.Ringing = true;
                    return true;
                }
            }

            this.Ringing = false;
            return false;
        }

        /**
         * Reset:
         * Resets all alarms at original state (original ring time and active)
         * Except if alarm is inactive
         */
        public void Reset()
        {
            foreach(Beeper alarm in Alarms)
            {
                if (alarm.State != AlarmState.Inactive)
                {
                    alarm.Reset();
                }
            }
        }

        /**
         * Snooze:
         * Asks each alarms to snooze (+10 minutes on ring time)
         */
        public void Snooze()
        {
            foreach(Beeper alarm in this.Alarms)
            {
                alarm.Snooze();
            }
        }

        /**
         * GetTime:
         * Returns an alarm's original time
         */
        public TimeSpan GetTime(Alarm alarm)
        {
            return this.Alarms[(int)alarm].OriginalTime;
        }

        /**
         * GetDays:
         * Returns an alarm's ring days
         */
        public Days GetDays(Alarm alarm)
        {
            return this.Alarms[(int)alarm].RingDays;
        }

        // Add 1 minute to ringtime
        public void IncreaseMinutes(Alarm alarm)
        {
            this.Alarms[(int)alarm].IncreaseMinutes();
        }

        // Take 1 minute to ringtime
        internal void DecreaseMinutes(Alarm alarm)
        {
            this.Alarms[(int)alarm].DecreaseMinutes();
        }

        // Add 1 hour to ringtime
        public void IncreaseHours(Alarm alarm)
        {
            this.Alarms[(int)alarm].IncreaseHours();
        }

        // Take 1 hour to ringtime
        internal void DecreaseHours(Alarm alarm)
        {
            this.Alarms[(int)alarm].DecreaseHours();
        }

        // Dynamically toggle ring days
        public void ToggleRingDay(Alarm alarm)
        {
            this.Alarms[(int)alarm].ToggleRingDay();
        }

        // Lock an alarm to avoid ring during alarm set.
        public void Lock(Alarm alarm)
        {
            this.Alarms[(int)alarm].Locked = true;
        }

        // Unlock an alarm to activate ring.
        public void Unlock(Alarm alarm)
        {
            this.Alarms[(int)alarm].Locked = false;
        }

        /**
         * NextTimezone:
         * Change timezone to the next one
         */
        public void NextTimezone()
        {
            int current_timezone = (int)this.Timezone;
            int next_timezone = current_timezone + 1;
            int max_value = Enum.GetNames(typeof(Timezone)).Length - 1;
            if (next_timezone > max_value)
                next_timezone = 0;
            this.Timezone = (Timezone)next_timezone;
        }

        /**
         * PreviousTimezone:
         * Change timezone to the previous one
         */
        public void PreviousTimezone()
        {
            int current_timezone = (int)this.Timezone;
            int next_timezone = current_timezone - 1;
            int max_value = Enum.GetNames(typeof(Timezone)).Length - 1;
            if (next_timezone < 0)
                next_timezone = max_value;
            this.Timezone = (Timezone)next_timezone;
        }
    }
}